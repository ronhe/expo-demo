import React from 'react';
import { View, TextInput, Button} from 'react-native';

import { sendMessage } from './db';


export default class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = { message: '' };
    }

    render() {
        return (
          <View>
              <TextInput
                  style={{
                      height: 40,
                      width: 200 ,
                      borderColor: 'gray',
                      borderWidth: 1
                  }}
                  onChangeText={message => this.setState({ message })}
              />
              <Button
                  title="Send"
                  onPress={() => sendMessage(this.state.message)}
              />
          </View>
        );
    }
}
