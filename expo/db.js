import firebase from 'firebase/app';
import 'firebase/database';


const firebaseConfig = {
  apiKey: "AIzaSyBUjDW7a-aTCcE8L2VJ3PESc44TtbcRN_U",
  authDomain: "expo-demo-spc.firebaseapp.com",
  databaseURL: "https://expo-demo-spc.firebaseio.com",
  projectId: "expo-demo-spc",
  storageBucket: "expo-demo-spc.appspot.com",
  messagingSenderId: "1057156967503",
  appId: "1:1057156967503:web:c2378ad0bed4bcb5808e3a"
};

firebase.initializeApp(firebaseConfig);
const db = firebase.database();


const sendMessage = async (message) => {
    if (!message) {
        return;
    }
    await db.ref('messages').push({
        timestamp: firebase.database.ServerValue.TIMESTAMP,
        text: message,
    });
}

export { sendMessage };