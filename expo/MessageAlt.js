import React, { useState } from 'react';
import { View, TextInput, Button} from 'react-native';

import { sendMessage } from './db';


function Message() {
    const [message, setMessage] = useState('');

    return (
        <View>
            <TextInput
                style={{
                    height: 40,
                    width: 200 ,
                    borderColor: 'gray',
                    borderWidth: 1
                }}
                onChangeText={text => setMessage(text)}
            />
            <Button
                title="Send"
                onPress={() => sendMessage(message)}
            />
        </View>
    );
}

export default Message;
