Expo Demo (mini) Workshop
=========================

Prerequisites
#############
Before we get started, please prepare your PC and smartphone
by following the steps below.

**PC**: I work with Windows (and cmder console), but any modern OS should do the work (Windows, Linux, OSX).

**Smartphone**: You need to get an iPhone (just kidding, Android is just fine).

#. Install `NodeJS <https://nodejs.org/en/>`_ version 12.
#. `Create an expo account <https://expo.io/signup>`_ (it's free).
#. Install Expo command line tool::

    npm install expo-cli --global

#. Sign in expo cli::

    expo signin
#. Create a new expo project::

    expo init <project-name> --template expo-template-blank@sdk-36
    cd <project-name>

#. Install firebase dependency::

    npm install firebase --save

#. Copy helper module `db.js <expo/db.js>`_ from this repo to your project's root directory.
#. Install the Expo client on your smartphone from the
   `App Store <https://itunes.apple.com/app/apple-store/id982107779>`_
   or from
   `Google Play <https://play.google.com/store/apps/details?id=host.exp.exponent&referrer=www>`_.

#. Run your project::

    expo start

   and preview it on your phone using the Expo client.
   It should look like this:

   .. image:: expo-blank-app-250.jpg

#. *Strongly recommended*: open the project in an IDE that supports React (JavaScript
   and JSX). I use PyCharm Professional (the community version won't do it), but Visual Studio Code
   should be a good free alternative.

That's it, you're good to go!

Write Your First App
####################
Your app should:

1. Allow the user to input a text message.
2. Write the message to a Firebase real-time cloud database,
   using the given helper module `db.js <expo/db.js>`_.

Your app should look something like this:

.. image:: expo-demo-app-250.png

If you succeed, the text sent from your app
would appear in the `Hall of Fame <https://expo-demo.ronhei.com/>`_.

Good luck!

Tips
$$$$

#. Hot reload is awesome - Try simple things first.
#. `TextInput reference <https://docs.expo.io/versions/latest/react-native/textinput/>`_
#. `Button reference <https://docs.expo.io/versions/latest/react-native/button/>`_
#. ``console.log('text', obj)``
#. Check out ``db.js`` to realize how to use it.

Walkthrough
$$$$$$$$$$$
Use this section if you're feeling lost.

#. Create a new module for your message sending component, for example ``Message.js``.
#. Inside ``Message.js``:
    #. Import ``React`` from ``react``.
    #. Import ``View, TextInput, Button`` from ``react-native``.
    #. Import ``sendMessage`` function from ``db.js``
    #. Define and export (default) a ``Message`` component that extends
       ``React.Component``.
    #. Add a ``constructor(props)`` method that initializes a state object
       with an empty ``message`` key.
    #. Add a ``render`` method that returns an empty ``View`` component.
    #. Add a ``TextInput`` component (inside the ``View`` component).
        #. Set its ``style`` property to make it visible:
            #. ``height``
            #. ``width``
            #. ``borderColor``
            #. ``borderWidth``
        #. Set its ``onChangeText`` property to an anonymous function
           that sets the state of ``message``.
    #. Add a ``Button`` component (inside the ``View`` component).
        #. Set its ``title`` property.
        #. Set its ``onPress`` property to an anonymous function
           that calls ``sendMessage``.
#. Inside ``App.js``:
    #. Import the ``Message`` component.
    #. Render a ``Message`` component in the ``App``'s output.

If you're still stuck,
my solution for this exercise is `here <expo>`_.


That's it, now give it a try!
